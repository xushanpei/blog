module.exports = {
  title: "前端开发爱好者",
  description: '专注分享前端相关技术文章、设计模式、数据结构、算法等',
  theme: "reco",
  base:'/blog/',
  head: [
    ['link', { rel: 'icon', href: '/images/favicon.ico' }]
  ],
  locales: {
    "/": {
      lang: "zh-CN",
    },
  },
  plugins: [
    ["@vuepress/nprogress"],
    [
      "sakura",
      {
        num: 15, // 默认数量
        show: true,
        zIndex: 99999,
        img: {
          replace: false, // false 默认图 true 换图 需要填写httpUrl地址
          httpUrl: "http://www.zpzpup.com/assets/image/sakura.png", // 绝对路径
        },
      },
    ],

    [
      "@vuepress-reco/vuepress-plugin-kan-ban-niang",
      {
        theme: ["blackCat"],
        clean: true,
        modelStyle: {
          position: "fixed",
          right: "65px",
          bottom: "0px",
          zIndex: 99999,
          pointerEvents: "none",
        },
      },
    ],
    [
      "@vuepress-reco/comments",
      {
        solution: "valine",
        options: {
          appId: "7NESrLw9KewVI9dyRFUWgS2G-gzGzoHsz",
          appKey: "dmG0vo6KLhgmngV80dQ5CSSR",
        },
      },
    ],
    [
      //动态标题
      "dynamic-title",
      {
        showText: "(/≧▽≦/)咦！欢迎回来！",
        hideText: "(●—●)喔哟，你快回来！",
        recoverTime: 2000,
      },
    ],
  ],
  themeConfig: {
    logo: "/images/logo.jpg",
    type: "blog",
    authorAvatar: "/images/logo.jpg",
    author: 'xy',
    search: true,
    searchMaxSuggestions: 10,
    lastUpdated: "Last Updated",
    record: "苏ICP备88888888号-8",
    startYear: "2021",
    nav: [{
        text: "首页",
        link: "/",
        icon: "reco-home",
      },
      {
        text: "设计模式",
        link: "/DesignPattern/index.md",
        icon: "reco-other",
      },
      {
        text: "数据结构",
        link: "/DataStructure/index.md",
        icon: "reco-document",
      },
      {
        text: "算法",
        link: "/Algorithm/index.md",
        icon: "reco-coding",
      },
      {
        text: "学习路线",
        link: "/Studly/index",
        icon: "reco-lock",
      },
      {
        text: "掘金",
        link: "https://juejin.cn/user/1116759545088190/posts",
        icon: "reco-juejin",
      },
      {
        text: "CSDN",
        link: "https://blog.csdn.net/qq_39398332",
        icon: "reco-csdn",
      },
      {
        text: "关于我",
        link: "/AboutMe/index.md",
        icon: "reco-account",
      },
    ],

    sidebar: {
      "/DesignPattern": [{
        title: "设计模式",
        collapsable: true,
        children: [{
          title: "工厂模式",
          path: "/DesignPattern/factory.md"
        }],
      }, ],

      "/DataStructure": [{
        title: "数据结构",
        collapsable: true,
        children: [{
          title: "数组详解",
          path: "/DataStructure/Array.md"
        },
        {
          title: "栈结构详解",
          path: "/DataStructure/Stack.md"
        },
        {
          title: "队列结构详解",
          path: "/DataStructure/Queue.md"
        }
      ],
      }, ],
    },
  },
};