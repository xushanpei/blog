---
title: 数据结构 - 数组详解
date: 2022-02-17
cover: https://s2.loli.net/2022/02/19/agpr2KNVPAZTXjW.jpg
tags:
 - 数据结构
categories:
 - 数据结构、算法、设计模式系列
---

![image](https://s2.loli.net/2022/02/19/agpr2KNVPAZTXjW.jpg)

> 哈喽,大家好，我是`xy`👨🏻‍💻. 这篇文章：`数据结构之数组结构详解` 将是我 `《javaScript数据结构》`系列的第一篇文章；2022年的Flag：`学习数据结构、算法、设计模式`，如果你也想和我一起学习，欢迎`加我为好友`，拉你进`数据结构算法设计模式学习群`一起学习交流❤️

## 前言

如果你之前学习过`javaScript`的基础知识，那么应该学习过`数组`,在基础知识中, 是要求我们灵活使用数组的, 所以大部分人应该对这部分没有问题, 但是这里我还是花费一些时间来带着大家一起学习一下`数组`.

因为`数据结构`中`数组`是其中很`重要`的一环。

## 什么是数组

- 是使用单独的变量名来存储一系列的值。

- 数组是一种类列表对象，它的原型中提供了遍历和修改元素的相关操作。

- JavaScript 数组的长度和元素类型都是非固定的。

- 数组的长度可随时改变，并且其数据在内存中也可以不连续，所以 JavaScript 数组不一定是密集型的，这取决于它的使用方式。

## 创建数组的三种方式（别说你不知道）

1. 使用`new`关键字创建数组：

```js
var arr = new Array();
arr[0] = 'Vue'
arr[1] = 'React'
arr[2] = 'Nodejs'
console.log(arr); 
// ['Vue','React','Nodejs']
```

2. 使用`new`关键字，将数组元素作为`参数传递`：

```js
var arr = new Array('Vue','React','Nodejs');
console.log(arr);
 // ['Vue','React','Nodejs']
```

3. 最简洁方式，推荐使用中括号`[]`创建数组：

```js
var arr = ['Vue','React','Nodejs'];
console.log(arr);
 // ['Vue','React','Nodejs']
```

## 数组基础常见操作

1. 通过length属性，获取数组长度
```js
var arr = ['Vue','React','Nodejs'];
console.log(arr.length); // 3
```

2. 通过索引访问数组元素
```js
var arr = ['Vue','React','Nodejs'];
console.log(arr[1]); // React
```

3. 遍历数组

```js
arr.forEach((item, index, array)=>{
  console.log(item, index)
})
// Vue 0
// React 1
// Nodejs 1
```

4. 添加元素到数组末尾
```js
var arr = ['Vue','React','Nodejs'];
let newLength = arr.push('Angular')
// ['Vue','React','Nodejs','Angular']
```

5. 添加元素到数组开头
```js
var arr = ['Vue','React','Nodejs'];
var newLength = arr.unshift('js')
// ['js','Vue','React','Nodejs']
```

6. 删除数组末尾元素
```js
var arr = ['Vue','React','Nodejs'];
var last = arr.pop() 
// ['Vue','React']
```

7. 删除数组开头的元素 
```js
var arr = ['Vue','React','Nodejs'];
var first = arr.shift() 
// ['React','Nodejs']
```
8. 删除数组任意位置的元素

```js
var arr = ['js','Vue','React','Nodejs','Angular'];
// 从下标2开始删除2个元素
var delList =  arr.splice(2, 2);
console.log(delList); 
// 返回的删除的元素List
// ['React', 'Nodejs']
console.log(arr); 
// 删除后的原数组
// ['js', 'Vue', 'Angular']
```
9. 删除并且插入数据

```js
var arr = ['js','Vue','React','Nodejs','Angular'];
// 从下标1开始删除3个，并且插入 's','d','f','p'
var oldList = arr.splice(1,3,'s','d','f','p');
console.log(oldList); 
// 返回的删除的元素List
// ['Vue', 'React', 'Nodejs']
console.log(arr)
// 删除插入后的原数组
// ['js', 's', 'd', 'f', 'p', 'Angular']
```

10. 数组数据截取

`slice()` 方法返回一个新的数组对象，这一对象是一个由 `begin` 和 `end` 决定的原数组的浅拷贝（包括 begin，不包括end）。原始数组不会被改变。

```js
var arr = ['js','Vue','React','Nodejs','Angular'];
var sliceArr = arr.slice(2,3);
console.log(sliceArr); 
// ['React']
```

## 数组的其它操作方法

>数组的方法其实还有很多，这里就不去一一介绍了，想要详细了解还有哪些方法，可以查看：`https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array`

| 方法名      |方法描述|
| :---------- | :--------------------------: |
| concat      |连接2个或更多数组，并返回结果|
| every       |对数组中的每一项运行给定函数，如果该函数对每一项都返回 true，则返回true, 否则返回false|
| filter      |对数组中的每一项运行给定函数，返回该函数会返回 true的项组成的数组|
| forEach     |对数组中的每一项运行给定函数。这个方法没有返回值|
| join        |将一个数组（或一个类数组对象）的所有元素连接成一个字符串并返回这个字符串。如果数组只有一个项目，那么将返回该项目而不使用分隔符|
| indexOf     |返回在数组中可以找到一个给定元素的第一个索引，如果不存在，则返回-1|
| lastIndexOf |返回指定元素（也即有效的 JavaScript 值或变量）在数组中的最后一个的索引，如果不存在则返回 -1|
| map         |map() 方法创建一个新数组，其结果是该数组中的每个元素是调用一次提供的函数后的返回值|
| reverse     |将数组中元素的位置颠倒，并返回该数组|
| slice       |返回一个新的数组对象，这一对象是一个由 begin 和 end 决定的原数组的浅拷贝|
| some        |测试数组中是不是至少有1个元素通过了被提供的函数测试。它返回的是一个Boolean类型的值|
| sort        |sort() 方法用原地算法对数组的元素进行排序，并返回数组|
| toString    |返回一个字符串，表示指定的数组及其元素|
| flat        |会按照一个可指定的深度递归遍历数组，并将所有元素与遍历到的子数组中的元素合并为一个新数组返回|
| reduce      |对数组中的每个元素执行一个由您提供的reducer函数(升序执行)，将其结果汇总为单个返回值|



## 写在最后

> `公众号`：`前端开发爱好者` 专注分享 `web` 前端相关`技术文章`、`视频教程`资源、热点资讯等，如果喜欢我的分享，给 🐟🐟 点一个`赞` 👍 或者 ➕`关注` 都是对我最大的支持。

欢迎`长按图片加好友`，我会第一时间和你分享`前端行业趋势`，`面试资源`，`学习途径`等等。

![user](https://s2.loli.net/2022/01/11/kUoLmdI9yu1gS7H.png)

关注公众号后，在首页：

- 回复`面试题`，获取最新大厂面试资料。
- 回复`简历`，获取 3200 套 简历模板。
- 回复`React实战`，获取 React 最新实战教程。
- 回复`Vue实战`，获取 Vue 最新实战教程。
- 回复`ts`，获取 TypeScript 精讲课程。
- 回复`vite`，获取 精讲课程。
- 回复`uniapp`，获取 uniapp 精讲课程。
- 回复`js书籍`，获取 js 进阶 必看书籍。
- 回复`Node`，获取 Nodejs+koa2 实战教程。
- 回复`数据结构算法`，获取 数据结构算法 教程。
- 回复`架构师`，获取 架构师学习资源教程。
- 更多教程资源应用尽有，欢迎`关注获取`