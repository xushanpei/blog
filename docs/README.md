---
home: true
heroText: 前端开发爱好者
tagline: '专注分享前端相关技术文章、设计模式、数据结构、算法等'
heroTextStyle: {
  color: 'red'
}
heroImage: /images/logo.jpg
bgImage: https://s2.loli.net/2022/01/11/I3TqzGxvw4BZnUp.jpg
bgImageStyle: {
  height: calc(100vh - 57px),
  # width: '100vw'
}
heroImageStyle: {
  maxHeight: '180px',
  display: block,
  margin: '2rem auto 1.5rem',
  borderRadius: '50%',
  boxShadow: '0 5px 18px rgba(0,0,0,0.2)'
}

isShowTitleInHome: false

---


<style>
.anchor-down {
  display: block;
  margin: 12rem auto 0;
  bottom: 45px;
  width: 20px;
  height: 20px;
  font-size: 34px;
  text-align: center;
  animation: bounce-in 2s 2s infinite;
  position: absolute;
  left: 50%;
  bottom: 20%;
  margin-left: -10px;
  cursor: pointer;
}
@-webkit-keyframes bounce-in{
  0%{transform:translateY(0)}
  20%{transform:translateY(0)}
  50%{transform:translateY(-20px)}
  80%{transform:translateY(0)}
  to{transform:translateY(0)}
}
.anchor-down::before {
  content: "";
  width: 20px;
  height: 20px;
  display: block;
  border-right: 3px solid #fff;
  border-top: 3px solid #fff;
  transform: rotate(135deg);
  position: absolute;
  bottom: 10px;
}
.anchor-down::after {
  content: "";
  width: 20px;
  height: 20px;
  display: block;
  border-right: 3px solid #fff;
  border-top: 3px solid #fff;
  transform: rotate(135deg);
}
.hero{
  z-index: 0;
  color: #FFF
}
</style>

<script>
export default {
  mounted () {
    const ifJanchor = document.getElementById("pointDown"); 
    ifJanchor && ifJanchor.parentNode.removeChild(ifJanchor);
    let a = document.createElement('a');
    a.id = 'pointDown';
    a.className = 'anchor-down';
    document.getElementsByClassName('hero')[0].append(a);
    let targetA = document.getElementById("pointDown");
    targetA.addEventListener('click', e => { // 添加点击事件
      this.scrollFn();
    })
  },

  methods: {
    scrollFn() {
      const windowH = document.getElementsByClassName('hero')[0].clientHeight; // 获取窗口高度
      console.log(windowH)
      document.documentElement.scrollTop = windowH; // 滚动条滚动到指定位置
    }
  }
}
</script>