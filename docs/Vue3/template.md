---
title: 5款非常优秀的基于 vue3.x 和 Naive UI 的中后台管理系统模板
date: 2022-01-03
cover: https://s2.loli.net/2022/01/11/NnlEKeY4MLmH7hd.png
sticky: 3
tags:
 - vue3.x
categories:
 - vue3.x系列
---

![](https://s2.loli.net/2022/01/11/NnlEKeY4MLmH7hd.png)

## 前言
> 哈喽,大家好 我是`xy`👨🏻‍💻。 在上一篇文章中详细的给大家讲解了如何使用 `Vite2 + Vue3 + TypeScript + Pinia` 搭建一套企业级的开发脚手架。今天给大家分享`5款`开源且非常优秀的基于 `vue3.x` 和 `Naive UI` 的中后台管理系统模板，助力大家快速开发一个中后台管理系统

## Naive UI Admin

仓库地址: `https://github.com/jekip/naive-ui-admin`

介绍: `Naive Ui Admin` 完全免费，且可商用，基于 `Vue3.0`、`Vite`、 `Naive UI`、`TypeScript` 的中后台解决方案，它使用了最新的前端技术栈，并提炼了典型的业务模型，页面，包括二次封装组件、动态菜单、权限校验、粒子化权限控制等功能，它可以帮助你快速搭建企业级中后台项目， 相信不管是从新技术使用还是其他方面，都能帮助到你。

- 二次封装实用高扩展性组件
- 响应式、多主题，多配置，快速集成，开箱即用
- 最新技术栈，使用 Vue3、Typescript、Pinia、Vite 等前端前沿技术
- 强大的鉴权系统，对路由、菜单、功能点等支持三种鉴权模式，满足不同的业务鉴权需求
- 持续更新，实用性页面模板功能和交互，随意搭配组合，让构建页面变得简单化


![](https://s2.loli.net/2022/01/11/NnlEKeY4MLmH7hd.png)


## Admin Work

仓库地址: `https://github.com/qingqingxuan/admin-work`

介绍: `Admin Work`，是 `Vue Admini Work` 系列中最成熟和完善的中后台框架，全面的系统配置，优质模板，常用组件，真正一站式开箱即用 采用时下最流行的 `Vue3 UI` 库--`NaiveUI`

- 使用了当今最流行的技术框架： Vue3 + Vite2 + Typescript + Navie UI
- 支持前端控制路由权限和后端控制路由模式
- 支持 mock ，完全独立于后台
- 提供了非常多的 mxin 代码块，方便集成各种功能
- 内置了大量常用的组件，比如，上传，消息提示等
- 支持多主题、多布局切换


![image](https://s2.loli.net/2022/01/11/DgRGQhLKPOEw6XT.png)


## Soybean Admin

仓库地址: `https://github.com/honghuangdc/soybean-admin`

介绍: `Soybean Admin` 是一个基于 `Vue3`、`Vite`、`Naive UI`、`TypeScript` 的免费中后台模版，它使用了最新的前端技术栈，内置丰富的插件，有着极高的代码规范，开箱即用的中后台前端解决方案，也可用于学习参考。

- 最新技术栈：使用 Vue3/vite2 等前端前沿技术开发, 使用高效率的npm包管理器pnpm
- TypeScript: 应用程序级 JavaScript 的语言
- 主题：丰富可配置的主题
- 代码规范：丰富的规范插件及极高的代码规范
- 路由配置：简易的路由配置


![](https://s2.loli.net/2022/01/11/BhOboSCvlpEcJkN.png)


## zce/fearless

仓库地址: `https://github.com/zce/fearless`

介绍: 一个基于 `Vue.js 3.x` & `TypeScript` & `Vite` 的管理平台`脚手架`，包含基本的身份认证和鉴权,相对来说比较纯净，可以直接使用


![](https://s2.loli.net/2022/01/11/yVZl84xXndjoT9m.png)


## tsx-naive-admin
仓库地址: `https://github.com/WalkAlone0325/tsx-naive-admin`

介绍: 基于` Vue 3` + `Naive UI` + `TSX` + `Vite` 的后台管理`基本模板`


![](https://s2.loli.net/2022/01/11/M6fmx98yg3RZV7z.png)


## 写在最后

> `公众号`：`前端开发爱好者` 专注分享 `web` 前端相关`技术文章`、`视频教程`资源、热点资讯等，如果喜欢我的分享，给 🐟🐟 点一个`赞` 👍 或者 ➕`关注` 都是对我最大的支持。

欢迎`长按图片加好友`，我会第一时间和你分享`前端行业趋势`，`面试资源`，`学习途径`等等。


![user](https://s2.loli.net/2022/01/11/kUoLmdI9yu1gS7H.png)


关注公众号后，在首页：

- 回复`面试题`，获取最新大厂面试资料。
- 回复`简历`，获取 3200 套 简历模板。
- 回复`React实战`，获取 React 最新实战教程。
- 回复`Vue实战`，获取 Vue 最新实战教程。
- 回复`ts`，获取 TypeAcript 精讲课程。
- 回复`vite`，获取 精讲课程。
- 回复`uniapp`，获取 uniapp 精讲课程。
- 回复`js书籍`，获取 js 进阶 必看书籍。
- 回复`Node`，获取 Nodejs+koa2 实战教程。
- 回复`数据结构算法`，获取 数据结构算法 教程。
- 回复`架构师`，获取 架构师学习资源教程。
- 更多教程资源应用尽有，欢迎`关注获取`