---
title: Vue3.x <script setup> 语法糖详解,助力快速上手Vue3.x
date: 2022-01-02
cover: https://s2.loli.net/2022/01/11/uXpoQbZWLOKJeks.jpg
sticky: 2
tags:
 - vue3.x
categories:
 - vue3.x系列
---

![](https://s2.loli.net/2022/01/11/uXpoQbZWLOKJeks.jpg)

> 哈喽,大家好 我是`xy`👨🏻‍💻。 在上一篇文章中，给大家详细讲解了如何快速使用 `vue3.x`，`typeScript`， `vite` 搭建一套企业级的开发脚手架；其中也提到了`<script setup>`语法，这篇文章就来给大家详细的讲讲 `<script setup>`语法糖，让你在使用 vue3 开发项目时更加得心应手 💪

## script setup 有哪些优势

`<script setup>` 是在单文件组件(SFC)中使用`组合式 API` 的编译时语法糖。相比于普通的`<script>`语法，它具有更多优势：

- 更少的样板内容，更`简洁`的代码。
- 能够使用纯`Typescript`声明 props 和抛出事件。
- 更好的运行时性能 (其模板会被编译成与其同一作用域的渲染函数，没有任何的中间代理)。
- 更好的 IDE `类型推断`性能 (减少语言服务器从代码中抽离类型的工作)。

## 基本语法

`<setup script>` 是 vue3 的一个新的语法糖，用起来特别简单。只需要在 script 标签中加上 setup 关键字。

`<setup script>` 中声明的`函数`、`变量`以及`import`引入的`内容、组件`都能在模板中直接使用：

```html
<template>
  <div>{{ msg }}</div>
  <br />
  <NButton @click="log">点击</NButton>
</template>

<script setup lang="ts">
import { NButton } from 'naive-ui'
const msg = '公众号：前端开发爱好者'

const log = () => {
  console.log(msg)
}
</script>
```

## 响应式

响应式状态需要明确使用`响应式 APIs` 来创建。和从 setup() 函数中返回值一样，ref 值在模板中使用的时候会自动解包：

```html
<template>
  <NButton @click="count++">点击++{{ count }}</NButton>
</template>

<script setup lang="ts">
import { NButton } from 'naive-ui'
import { ref } from 'vue'

const count = ref<number>(0)
</script>
```

更多响应式 APIs 的使用请查阅官方文档：`https://v3.cn.vuejs.org/api/basic-reactivity.html#reactive`,本文就不做一一讲解。

## 动态组件

在 `setup script` 中要使用动态组件的时候，用动态的 `:is` 来绑定：

```html
<template>
  <component :is="isShow ? LoginVue : null" />
  <NButton @click="setIsShow">是否展示：{{ isShow }}</NButton>
</template>

<script setup lang="ts">
import { ref } from "vue"
import LoginVue from "../login/Login.vue";
import { NButton } from 'naive-ui'

let isShow = ref<boolean>(false)

const setIsShow = ()=>{
  isShow.value = !isShow.value
}
</script>
```

效果如下：

![](https://s2.loli.net/2022/01/11/dl1uTGPKCAiO3YS.gif)


## 组件数据传递(props 和 emits)

在 `<script setup>` 中必须使用 `defineProps` 和 `defineEmits` API 来声明 props 和 emits ，它们具备完整的`类型推断`并且在`<script setup>`中是直接可用的

### 定义组件的 props

通过`defineProps`指定当前 `props` 类型，获得上下文的 props 对象。示例：

```html
<script setup lang="ts">
  const props = defineProps({
    num: Number
  })
</script>
```

### 定义组件的 emits

使用`defineEmit`定义当前组件含有的`事件`，并通过返回的上下文去执行 emit。示例：

```html
<script setup lang="ts">
  const emits = defineEmits(['addNum'])
</script>
```

父组件：

```html
<template>
  <NButton @click="addNum">我是父组件，点击 ++ {{ num }}</NButton>
  <hr />
  <Child :num="num" @addNum="addNum" />
</template>

<script setup lang="ts">
import Child from './Child.vue'
import { NButton } from 'naive-ui'
import { ref } from 'vue'

const num = ref<number>(0)

const addNum = () => {
  num.value++
}
</script>

```

子组件：

```html
<template>
  <div>{{ props.num }}</div>
  <NButton @click="emits('addNum')">修改父组件num{{ props.num }}</NButton>
  <hr />
</template>

<script setup lang="ts">
import { NButton } from 'naive-ui'
const props = defineProps({
  num: Number,
})

const emits = defineEmits(['addNum'])
</script>
```

效果如下：

![](https://s2.loli.net/2022/01/11/aFmjvG4MEDI3eXA.gif)


### 对外暴露属性(defineExpose)

`<script setup>` 的组件默认不会对外部暴露任何内部声明的属性。
如果有部分属性要暴露出去，可以使用 `defineExpose`

说白了就是暴露属性给外部使用

父组件：

```html
<template>
  <NButton @click="logChildren">我是父组件</NButton>
  <hr />
  <Child ref="child" />
</template>

<script setup lang="ts">
import Child, { ChildType } from './Child.vue'
import { NButton } from 'naive-ui'
import { ref } from 'vue'

const child = ref<ChildType>(null)

const logChildren = () => {
  console.log(child.value.title)
}
</script>
```

子组件：

```html
<template>
  <div>我是子组件</div>
</template>

<script setup lang="ts">
import { ref } from 'vue'
const title = ref<string>('我是子组件的title')

export interface ChildType {
  title?: String
}

defineExpose({
  title,
})
</script>
```

效果如下：

![](https://s2.loli.net/2022/01/11/C3B9gaTxzVpn4qJ.png)


> ⭐️ 注意： `defineProps`、`defineEmits`、`defineEmits` API 不需要引入可以直接使用

## 获取 slots 和 attrs

> 注：`useContext` API 被弃用，取而代之的是更加细分的 api: `useSlots` 和 `useAttrs`。

在 `<script setup>`使用 slots 和 attrs 的情况应该是很罕见的，因为可以在模板中通过 `$slots` 和 `$attrs` 来访问它们。在你的确需要使用它们的罕见场景中，可以分别用 `useSlots` 和 `useAttrs` 两个辅助函数：

```html
<script setup lang="ts">
import { useSlots, useAttrs } from 'vue'

const slots = useSlots()
const attrs = useAttrs()
</script>
```

`useSlots` 和 `useAttrs` 是真实的运行时函数，它会返回与 `setupContext.slots` 和 `setupContext.attrs` 等价的值，同样也能在普通的组合式 `API` 中使用

## 创建异步 setup 方法(顶层 await)

`<script setup>`语法的另一个很酷的功能是创建异步 setup 非常容易。这对于在创建组件时加载`api`，甚至将代码绑定到`<suspense>`功能很有用。

我们所要做的就是让我们的 setup 函数是异步的，在我们的 `<script setup>` 中使用一个`顶级的await`。

`<script setup>` 中可以使用顶层 await。结果代码会被编译成 `async setup()`

例如，如果我们使用的是 `Fetch API`，我们可以像这样使用 `await`:
```html
<script setup>   
   const post = await fetch(`/api/pics`).then((a) => a.json())
</script>
```


这样setup()函数将是异步的。

[](https://v3.cn.vuejs.org/api/sfc-script-setup.html#%E5%9F%BA%E6%9C%AC%E8%AF%AD%E6%B3%95。 "script setup 官方文档")

## 写在最后

> `公众号`：`前端开发爱好者` 专注分享 `web` 前端相关`技术文章`、`视频教程`资源、热点资讯等，如果喜欢我的分享，给 🐟🐟 点一个`赞` 👍 或者 ➕`关注` 都是对我最大的支持。

欢迎`长按图片加好友`，我会第一时间和你分享`前端行业趋势`，`面试资源`，`学习途径`等等。

![user](https://s2.loli.net/2022/01/11/kUoLmdI9yu1gS7H.png)

关注公众号后，在首页：

- 回复`面试题`，获取最新大厂面试资料。
- 回复`简历`，获取 3200 套 简历模板。
- 回复`React实战`，获取 React 最新实战教程。
- 回复`Vue实战`，获取 Vue 最新实战教程。
- 回复`ts`，获取 TypeAcript 精讲课程。
- 回复`vite`，获取 精讲课程。
- 回复`uniapp`，获取 uniapp 精讲课程。
- 回复`js书籍`，获取 js 进阶 必看书籍。
- 回复`Node`，获取 Nodejs+koa2 实战教程。
- 回复`数据结构算法`，获取 数据结构算法 教程。
- 回复`架构师`，获取 架构师学习资源教程。
- 更多教程资源应用尽有，欢迎`关注获取`
