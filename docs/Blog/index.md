---
title: 2022年的Flag从搭建博客开始，设计模式,数据结构,算法汇总【手摸手系列】
date: 2022-01-03
cover: https://s2.loli.net/2022/01/11/Y6anC4jVI8xvbwF.jpg
sticky: 4
tags:
 - VuePress
categories:
 - 博客搭建
---

![0](https://s2.loli.net/2022/01/11/Y6anC4jVI8xvbwF.jpg)

> 哈喽,大家好 我是`xy`👨🏻‍💻。 从去年年中开始正式写`公众号`，到现在有半年之久了，其实写文章的过程也是不断的`复盘`过程，能够发现自己收获了哪些以及哪些地方的不足，于是在`2022`年，打算搭建一个`博客`，用来记录自己在未来一年的`学习历程`，同时也给自己立了几个`Flag`,希望自己在`2022`年更加努力一点。以下就`手把手`教大家搭建一个属于自己的`博客系统`吧 💪

## 技术选型

在搭建博客之前，了解到市面上搭建博客的技术方案有很多，比如 `WordPress`，`heox`，`dumi`，`vuepress`，`vitepress`...

经过对比，最终选择了 `vuepress`，那么我们来看下 `vuepress` 有哪些优势：

- `简洁至上`：以 `Markdown` 为中心的项目结构，以最少的配置帮助你专注于写作。
- `Vue 驱动`：享受 `Vue + webpack` 的开发体验，可以在 Markdown 中使用 Vue 组件，又可以使用 Vue 来开发自定义主题。
- `高性能`：VuePress 会为每个页面预渲染生成静态的 `HTML`，同时，每个页面被加载的时候，将作为 `SPA` 运行。
- `生态`：强大的生态，基于 vuepress 的各种`插件`以及`主题`配置，可以丰富你的博客系统

## 博客初始化

1. 新建一个文件夹 `blog`， `cd` 进入文件夹下，使用你喜欢的`包管理器`进行初始化

```bash
# yarn
yarn init
# npm
npm init
```

2. 安装 `vuepress`

```bash
# yarn
yarn add -D vuepress
# npm
npm install -D vuepress
```

3. 在 `blog` 下创建文件夹 `docs`，`VuePress` 会以 `docs` 为文档根目录，docs 下创建文件 `README.md`将会默认为博客的`主页`

![](https://s2.loli.net/2022/01/11/CutGDyBKaUc63T2.png)

1. 在 `package.json` 中添加一些 `scripts`，用于启动和打包：

```js
"scripts": {
    "dev": "vuepress dev docs .",
    "build": "vuepress build docs ."
  },
```

5. 在本地启动服务器

```bash
yarn dev
```

`VuePress` 会在 `http://localhost:8080` (opens new window)启动一个热重载的开发服务器。

## 基础配置

> 增加`配置文件`和`标题`配置等

1. 在 `docs` 文件夹下增加 `.vuepress` 文件夹

![](https://s2.loli.net/2022/01/11/UeLQ4hatSduoR1N.png)

1. 在 `.vuepress` 文件夹下添加 `config.js`，配置网站的`标题`和`描述`

![](https://s2.loli.net/2022/01/11/tN7VRbqPLlr4An3.png)

```js
module.exports = {
  title: '前端开发爱好者',
  description: '专注分享前端相关技术文章、设计模式、数据结构、算法等'
}
```

1. 重新启动查看效果

![](https://s2.loli.net/2022/01/11/AQc6lB48h1F7TKj.png)

## 主题配置

> 主题选用比较流行的 `vuepress-theme-reco`

1. 安装

```bash
npm install vuepress-theme-reco --save-dev
# or
yarn add vuepress-theme-reco
```

2. 使用

```js
// .vuepress/config.js
module.exports = {
  title: '前端开发爱好者',
  description: '专注分享前端相关技术文章、设计模式、数据结构、算法等'
  theme: 'reco'
}
```

3. 重新启动查看效果

![](https://s2.loli.net/2022/01/11/hwxvbM3k6i9fQIo.png)

![](https://s2.loli.net/2022/01/11/vXZwKkrVibBJeI4.png)

发现启动页面增加的吃豆人的`loading`效果以及主页面导航栏增加了`主题切换`功能

## 添加导航栏和侧边栏

### 导航栏配置

在`config.js`中增加配置 `themeConfig`=> `nav`，具体配置如下：

```js
module.exports = {
  title: '前端开发爱好者',
  description: '专注分享前端相关技术文章、设计模式、数据结构、算法等',
  theme: 'reco',
  themeConfig: {
    nav: [{
        text: "首页",
        link: "/",
        icon: "reco-home",
      },
      {
        text: "设计模式",
        link: "/DesignPattern/index.md",
        icon: "reco-other",
      },
      {
        text: "数据结构",
        link: "/DataStructure/index.md",
        icon: "reco-document",
      },
      {
        text: "算法",
        link: "/Algorithm/index.md",
        icon: "reco-coding",
      },
      {
        text: "学习路线",
        link: "/Studly/index",
        icon: "reco-lock",
      },
      {
        text: "掘金",
        link: "https://juejin.cn/user/1116759545088190/posts",
        icon: "reco-juejin",
      },
      {
        text: "CSDN",
        link: "https://blog.csdn.net/qq_39398332",
        icon: "reco-csdn",
      },
      {
        text: "关于我",
        link: "/AboutMe/index.md",
        icon: "reco-account",
      },
    ],
  }
}
```

`icon` 直接使用的 `vuepress-theme-reco` 自带的一些配置，具体配置可以查看官方：`https://vuepress-theme-reco.recoluan.com/views/1.x/configJs.html`

效果如下展示：

![](https://s2.loli.net/2022/01/11/1ScFiJ9Kjez6Vko.png)

到这一步导航栏基本配置完成，但是点击链接会发现 `404`，这个时候直接去新建对应的文件就行了，`link` 对应的其实就是 `docs` 文件所在的目录

![image](https://s2.loli.net/2022/01/11/olCdc9x3PJrnhgV.png)

把我之前准备好的 `markdown` 文件直接拷贝过来:

![](https://s2.loli.net/2022/01/11/LfdQAoFvihtIcbq.png)

重新启动项目，点击 `学习路线`

![image](https://s2.loli.net/2022/01/11/CbBgIzMHm1hx6po.png)

### 侧边栏配置

在`config.js`中增加配置 `themeConfig`=> `sidebar`， `sidebar`和`nav`平级，具体配置如下：

```js
sidebar: {
      "/DesignPattern": [{
        title: "设计模式",
        collapsable: true,
        children: [{
          title: "工厂模式",
          path: "/DesignPattern/factory.md"
        }],
      }, ],
    },
```

注意目录要一一对应，想在哪个导航下增加子菜单`sidebar`中的`key`就配置哪一个

![image](https://s2.loli.net/2022/01/11/pjY7W3NOlF8iLhn.png)

重新启动，点击导航中的 `设计模式` :

![image](https://s2.loli.net/2022/01/11/hK2zOVfR4uNtpSy.png)

## 首页配置

1. 直接修改 `docs` 下的 `README.md` 文件，配置内容如下:

```
---
home: true
heroText: 前端开发爱好者
tagline: '专注分享前端相关技术文章、设计模式、数据结构、算法等'
heroTextStyle: {
  color: 'red'
}
heroImage: /images/logo.jpg
bgImage: /images/homeBg.jpg
bgImageStyle: {
  height: calc(100vh - 57px),
  # width: '100vw'
}
heroImageStyle: {
  maxHeight: '180px',
  display: block,
  margin: '2rem auto 1.5rem',
  borderRadius: '50%',
  boxShadow: '0 5px 18px rgba(0,0,0,0.2)'
}

isShowTitleInHome: false

---


<style>
.anchor-down {
  display: block;
  margin: 12rem auto 0;
  bottom: 45px;
  width: 20px;
  height: 20px;
  font-size: 34px;
  text-align: center;
  animation: bounce-in 2s 2s infinite;
  position: absolute;
  left: 50%;
  bottom: 20%;
  margin-left: -10px;
  cursor: pointer;
}
@-webkit-keyframes bounce-in{
  0%{transform:translateY(0)}
  20%{transform:translateY(0)}
  50%{transform:translateY(-20px)}
  80%{transform:translateY(0)}
  to{transform:translateY(0)}
}
.anchor-down::before {
  content: "";
  width: 20px;
  height: 20px;
  display: block;
  border-right: 3px solid #fff;
  border-top: 3px solid #fff;
  transform: rotate(135deg);
  position: absolute;
  bottom: 10px;
}
.anchor-down::after {
  content: "";
  width: 20px;
  height: 20px;
  display: block;
  border-right: 3px solid #fff;
  border-top: 3px solid #fff;
  transform: rotate(135deg);
}
.hero{
  z-index: 0;
  color: #FFF
}
</style>

<script>
export default {
  mounted () {
    const ifJanchor = document.getElementById("pointDown");
    ifJanchor && ifJanchor.parentNode.removeChild(ifJanchor);
    let a = document.createElement('a');
    a.id = 'pointDown';
    a.className = 'anchor-down';
    document.getElementsByClassName('hero')[0].append(a);
    let targetA = document.getElementById("pointDown");
    targetA.addEventListener('click', e => { // 添加点击事件
      this.scrollFn();
    })
  },

  methods: {
    scrollFn() {
      const windowH = document.getElementsByClassName('hero')[0].clientHeight; // 获取窗口高度
      console.log(windowH)
      document.documentElement.scrollTop = windowH; // 滚动条滚动到指定位置
    }
  }
}
</script>
```

注意`背景图片`和 `logo` 图片，我是直接放在项目中的；在 `.vuepress` 下新建一个 `public` 文件夹，用来存放静态文件：

![image](https://s2.loli.net/2022/01/11/DakuO38rLK7s15g.png)

1. 修改 config.js 文件，在`themeConfig`增加一些自定义信息配置

```js
themeConfig: {
    logo: "/images/logo.jpg",
    type: "blog",
    authorAvatar: "/images/logo.jpg",
    author: 'xy',
    search: true,
    searchMaxSuggestions: 10,
    lastUpdated: "Last Updated",
    record: "苏ICP备88888888号-8",
    startYear: "2021",
  }
```

3. 重新启动查看首页效果

![](https://s2.loli.net/2022/01/11/Y6anC4jVI8xvbwF.jpg)

![](https://s2.loli.net/2022/01/11/f5sv4zt9CPUpHDF.jpg)


到这里一个`博客`基本上配置完成了，但是细心的同学应该会发现，部分地方展示的是`英文`,修改 `config.js` 文件增加配置：

```
locales: {
    "/": {
      lang: "zh-CN",
    },
  },
```

![](https://s2.loli.net/2022/01/11/8uYCwULr61m7EiS.jpg)

## 插件配置

> `vuepress` 强大的生态，有着各种丰富的插件可以选择，丰富你的博客,这里给大家推荐几款花里胡哨的插件，当然了，官方也给大家推荐了很多插件，具体自行查看官方文档

1. vuepress `樱花插件`

```bash
yarn add vuepress-plugin-sakura
```

```js
// 只要把这个放进 config的plugins中就可以了 有木有很简单
 ["sakura", {
        num: 20,  // 默认数量
        show: true, //  是否显示
        zIndex: -1,   // 层级
        img: {
          replace: false,  // false 默认图 true 换图 需要填写httpUrl地址
          httpUrl: '...'     // 绝对路径
        }
    }]
```

2. vuepress `看板娘插件`

```bash
yarn add @vuepress-reco/vuepress-plugin-kan-ban-niang
```

```js
 [
      "@vuepress-reco/vuepress-plugin-kan-ban-niang",
      {
        theme: ["blackCat"],
        clean: true,
        modelStyle: {
          position: "fixed",
          right: "65px",
          bottom: "0px",
          zIndex: 99999,
          pointerEvents: "none",
        },
      },
    ],
```

3. vuepress `留言板插件`

```bash
yarn add @vuepress-reco/comments
```

```js
 [
      "@vuepress-reco/comments",
      {
        solution: "valine",
        options: {
          appId: "",
          appKey: "",
        },
      },
    ],
```

`appId`和`appKey`的获取直接到`leancloud`中申请获取：`https://console.leancloud.cn/login?from=%2Fapps`

4. vuepress `动态标题`

```bash
yarn add vuepress-plugin-dynamic-title
```

```js
 [
      "dynamic-title",
      {
        showIcon: "/favicon.ico",
        showText: "(/≧▽≦/)咦！又好了！",
        hideIcon: "/failure.ico",
        hideText: "(●—●)喔哟，崩溃啦！",
        recoverTime: 2000
      }
    ]
```

还有很多插件：

官方插件：`https://vuepress-theme-reco.recoluan.com/views/plugins/`

插件广场：`https://vuepress-theme-reco.recoluan.com/views/other/recommend.html`

## 几个问题

### 1.关于图片

虽然图片可以直接放在`public`文件夹下，但是这样文章多了，打包的资源肯定就会很大，于是我的博客里使用了开源的图床`sm.sm`.
一开始使用的 `gitee` 搭建的图床,浏览器端展示没问题,但是在ios微信内置浏览器打开图片无法展示,所以就直接使用 `sm.sm`了

### 2.首页文章列表和文中的不一样

![](https://s2.loli.net/2022/01/11/GTtu8o1CjZ2EepI.jpg)

这里是自己修改了主题的源码文件，然后通过使用`patch-package`对源码进行补丁处理，使得每次重新安装依赖不用再次修改源码

当然了，具体如何修改，可以加我`文末的微信`，我肯定不会吝啬的分享给你的

### 3.部署的问题

部署我是直接使用的 `giteePages` ，方便快捷，也省去了购买服务器的钱，当然了，部署的方式有很多，这个根据自己的实际情况选择。 

在 `config.js` 增加以下配置: 

```js
module.exports = {
    // base 设置的是你的仓库名称
    base:'/blog/',
}
```

![](https://s2.loli.net/2022/01/11/ThdG1iLxoVMADez.png)

## Flag

在博客的导航栏上，比较重要的就是 `设计模式`,`数据结构`,`算法`;这也是我搭建这个博客的初衷，希望自己在2022年能够多去研究下这些，同时也给自己设定一个小目标，在2022年结尾，希望在每个类的下面增加`20`篇文章，也就是一共`60`篇。当然了，想要和我一起学习的小伙伴，可以加我`文末的微信`，2022，我们一起冲💪💪💪

最后给大家奉上我的博客地址吧

>地址：`https://xushanpei.gitee.io/blog/`

## 写在最后

> `公众号`：`前端开发爱好者` 专注分享 `web` 前端相关`技术文章`、`视频教程`资源、热点资讯等，如果喜欢我的分享，给 🐟🐟 点一个`赞` 👍 或者 ➕`关注` 都是对我最大的支持。

欢迎`长按图片加好友`，我会第一时间和你分享`前端行业趋势`，`面试资源`，`学习途径`等等。


![user](https://s2.loli.net/2022/01/11/kUoLmdI9yu1gS7H.png)


关注公众号后，在首页：

- 回复`面试题`，获取最新大厂面试资料。
- 回复`简历`，获取 3200 套 简历模板。
- 回复`React实战`，获取 React 最新实战教程。
- 回复`Vue实战`，获取 Vue 最新实战教程。
- 回复`ts`，获取 TypeAcript 精讲课程。
- 回复`vite`，获取 精讲课程。
- 回复`uniapp`，获取 uniapp 精讲课程。
- 回复`js书籍`，获取 js 进阶 必看书籍。
- 回复`Node`，获取 Nodejs+koa2 实战教程。
- 回复`数据结构算法`，获取 数据结构算法 教程。
- 回复`架构师`，获取 架构师学习资源教程。
- 更多教程资源应用尽有，欢迎`关注获取`
